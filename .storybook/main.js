import { configure } from "@storybook/html";
import Twig from "twig";
import twigDrupal from "twig-drupal-filters";

// Load our Tailwind CSS
import "./src/css/main.css";

// Add the filters to Twig instance.
twigDrupal(Twig);

configure(
  require.context("../src/storybook-components", true, /\.stories\.js$/),
  module
);

export default {
  stories: ['../src/storybook-components/**/*.stories.@(js|mdx)'],
  addons: ['@storybook/addon-docs'],
};