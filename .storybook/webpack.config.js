const { resolve } = require("path");

module.exports = ({ config }) => {
  // Twig:
  config.resolve.alias["@templates"] = resolve(
    __dirname,
    "../src/storybook-components"
  );
  config.resolve.extensions.push(".twig");
  config.module.rules.push({
    test: /\.twig$/,
    use: [
      {
        loader: "twigjs-loader",
      },
    ],
  });

  // PostCSS:
  config.module.rules.push({
    test: /\.css$/,
    use: [
      {
        loader: "postcss-loader",
      },
    ],
  });


  return config;
};
