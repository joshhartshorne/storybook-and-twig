// Images import automatically
require.context("../img/", true, /\.(png|svg|jpg|gif)$/);

/*
 * Stylesheet imports
 */
import "../css/main.css";

/*
 * Site modules
 * See js/modules
 */

import accordion from "../storybook-components/accordion/accordion.js";
accordion();
