import { useEffect } from "@storybook/client-api";
import Button from "./button.twig";
import "./button.css";

export default {
  title: "Button",
  component: Button,
  parameters: {
    description: 'Here is a description',
  }
};

export const BaseButton = () => {
  useEffect(() => {
    console.log("Button");
  }, []);

  return Button({
    title: "This is a label",
  });
};
