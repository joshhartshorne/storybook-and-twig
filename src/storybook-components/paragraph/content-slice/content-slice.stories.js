import { useEffect } from "@storybook/client-api";
import ContentSlice from "./content-slice.twig";
import "./content-slice.css";
import accordionJS from "../../accordion/accordion.js";

export default {
  title: "Content Slice",
  component: ContentSlice,
};

export const BaseSlice = () => {
  useEffect(() => {
    accordionJS();
  }, []);

  return ContentSlice({
    data: {
      title: "Here is a title for the slice",
      content: "Here is the content",
      button_title: "Button title goes here",
      accordion_items: [
        {
          title: "Accordion Item One!",
          content:
            "<p>Praesent ac massa at ligula laoreet iaculis. Phasellus gravida semper nisi. Morbi nec metus.</p>",
          id: "1",
        },
        {
          title: "Accordion Item Two",
          content:
            "<p>Phasellus accumsan cursus velit. Etiam iaculis nunc ac metus. Vestibulum fringilla pede sit amet augue.</p><p>Morbi ac felis. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Nunc sed turpis.</p>",
          id: "2",
        },
      ],
    },
  });
};
