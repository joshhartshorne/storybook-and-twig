import Accordion from "./accordion.twig";
import "./accordion.css";

export default {
  title: "Accordion",
  component: Accordion,
};

export const BaseAccordion = () => {
  return Accordion({
    data: {
      accordion_items: [
        {
          title: "Accordion Item One!",
          content:
            "<p>Praesent ac massa at ligula laoreet iaculis. Phasellus gravida semper nisi. Morbi nec metus.</p>",
          id: "1",
        },
        {
          title: "Accordion Item Two",
          content:
            "<p>Phasellus accumsan cursus velit. Etiam iaculis nunc ac metus. Vestibulum fringilla pede sit amet augue.</p><p>Morbi ac felis. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Nunc sed turpis.</p>",
          id: "2",
        },
      ],
    },
  });
};
